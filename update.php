<?php

function get_link_id_by_url($url) {
    $squery = $db->prepare('SELECT id FROM links WHERE link = :link');
    $squery->bindParam(':link', $url, PDO::PARAM_STR, 255);
    $squery->execute();
    
    $result = $squery->fetch(PDO::FETCH_ASSOC);
    
    return $result;
}

function get_votes_by_link_id($link_id) {
    $squery = $db->prepare('SELECT votes FROM votes WHERE link_id = :link_id');
    $squery->bindParam(':link_id', $url, PDO::PARAM_INT);
    $squery->execute();
    
    $result = $squery->fetch(PDO::FETCH_ASSOC);
    
    return unserialize($result);
}
    

function update_vote_by_url($url, $category_id, $op = 'SUM') {
    $link_id = get_link_id_by_url($url);
    $votes = get_votes_by_link_id($link_id);

    switch ($op) {
        case 'SUM':
            $votes[$category_id] = $votes[$category_id]++;
            break;
        
        case 'SUB':
            $votes[$category_id] = $votes[$category_id]--;
            break;
    }
    
    $votes = serialize($votes);

    $squery = $db->prepare('UPDATE votes SET votes = :votes WHERE link_id = :link_id');
    $squery->bindParam(':votes', $votes);
    $squery->bindParam(':link_id', $link_id);
    $squery->execute();
}