<?php

require_once(__DIR__ . '/../bootstrap.php');

if (ENVIRONMENT === 'dev')
{
    $app = new \Slim\App([
        'settings' => [
            'displayErrorDetails' => true
        ]
    ]);
}
else
{
    $app = new Slim\App();
}

// Define app routes below
require_once(__DIR__ . '/routes.php');

$app->run();
