<?php

$app->get('/', function (Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
  $homeController = new HomeController($request, $response, $args);
  return $homeController->index();
});

// @TODO: get link info.
$app->post('/retrieve/news-status', function (Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
  $link = $_POST['url'];
  // @TODO: delete that hard-coded link!
  //$link = 'http://br.blastingnews.com/tv-famosos/2017/10/dupla-maiara-e-maraisa-e-vitimada-em-acidente-aereo-que-tristeza-002092345.html';

  $news = new News($request, $response, $args, $link);
  return $news->getInfo($link);
});

// @TODO: get link and vote info.
$app->post('/update/vote', function (Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
  $link = $_POST['url'];
  $category = $_POST['category'];

  $votes = new Votes($request, $response, $args, $link, $category);
  return $votes->updateVote($link, $category);
});
