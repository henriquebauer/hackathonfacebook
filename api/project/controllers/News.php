<?php

class News {
  private $id;
  public $link;
  private $domain;
  private $db;
  private $m_request;
  private $m_response;
  private $m_args;

  public function __construct(\Slim\Http\Request $request, \Slim\Http\Response $response, $args, $link) {
    $this->m_request = $request;
    $this->m_response = $response;
    $this->m_args = $args;

    $this->db = new mysqli("204.11.58.87", "hackatho_face", "renuga", "hackatho_facebook");
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getLink() {
    return $this->link;
  }

  public function setLink($link) {
    $this->link = $link;
  }

  public function getDomain() {
    return $this->domain;
  }

  public function setDomain($domain) {
    $this->domain = $domain;
  }

  public function currentStatus() {
    // Output the link information.
    // Query the database table (link), inner join with votes table and unserialize it to display.
  }

  function getInfo($link) {
    // Carrega as informacoes da noticia.
    $link_id = $this->getLinkIdByUrl($link);
    $votes_obj = new Votes($this->m_request, $this->m_response, $this->m_args);
    $votes = $votes_obj->getVotesByLinkId($link_id);

    // Calcula as informacoes da noticia.
    $categories = new Categories();
    $news_veracity = $categories->getCategoryById(array_search(max($votes), $votes));

    // Carrega as informacoes do site.
    $url_parsed = parse_url($link);
    $domain = str_replace('www.', '', $url_parsed['host']);

    $query = sprintf('SELECT id FROM links WHERE domain = "%s"', $domain);
    $result = $this->db->query($query);
    while ($row = $result->fetch_assoc()) {
      $results[] = $row['id'];
    }

    $link_ids = implode(',', $results);
    $votes_per_domain = $votes_obj->getVotesByLinkIds($link_ids);

    $qty_votes = array();
    foreach ($votes_per_domain as $vote) {
      $votes_per_link = unserialize($vote);
      foreach ($votes_per_link as $category_id => $qty) {
        $qty_votes[$category_id] = !empty($qty_votes[$category_id]) ? ($qty_votes[$category_id] + $qty) : $qty;
      }
    }

    // Calcula as informaçoes do site.
    $site_veracity = $categories->getCategoryById(array_search(max($qty_votes), $qty_votes));

    $message = '<p class="status-%s site">Este site foi avaliado como <span>%s</span></p>';
    $message .= '<p class="status-%s content">Este conteúdo foi avaliado como <span>%s</span></p>';
    $body = sprintf($message, strtolower($site_veracity), $site_veracity, strtolower($news_veracity), $news_veracity);

    return $this->m_response->getBody()->write($body);
  }


  public function getLinkIdByUrl($link) {
    $query = sprintf('SELECT id FROM links WHERE link = "%s"', $link);
    $result = $this->db->query($query);

    return $result->fetch_assoc()['id'];
  }

}
