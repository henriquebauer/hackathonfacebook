<?php

class Categories {
  private $id;
  private $category;
  private $db;

  public function __construct() {
    $this->db = new mysqli("204.11.58.87", "hackatho_face", "renuga", "hackatho_facebook");;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getCategory() {
    return $this->category;
  }

  public function setCategory($category) {
    $this->category = $category;
  }

  function getCategoryById($category_id) {
    $query = sprintf('SELECT category FROM categories WHERE id = %s', $category_id);
    $result = $this->db->query($query);

    return $result->fetch_assoc()['category'];
  }

}
