<?php

class Votes {
  private $linkId;
  private $serializedVote;
  private $connection;
  private $db;
  private $m_request;
  private $m_response;
  private $m_args;

  public function __construct(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
    $this->m_request = $request;
    $this->m_response = $response;
    $this->m_args = $args;

    $this->db = new mysqli("204.11.58.87", "hackatho_face", "renuga", "hackatho_facebook");
  }

  public function updateVote($url, $category_id, $op = 'SUM') {
    $news = new News($this->m_request, $this->m_response, $this->m_args, $url);
    $link_id = $news->getLinkIdByUrl($url);
    $votes = $this->getVotesByLinkId($link_id);
    
    $count = $votes[$category_id];
    switch ($op) {
      case 'SUM':
        $votes[$category_id] = ($count + 1);
        break;

      case 'SUB':
        $votes[$category_id] = ($count - 1);
        break;
    }
    $votes = serialize($votes);

    $query = sprintf('UPDATE votes SET votes = "%s" WHERE link_id = "%s"', $votes, $link_id);
    $result = $this->db->query($query);

    $message = '<p class="vote">Voto computado com sucesso!</p>';

    return $this->m_response->getBody()->write($message);
  }

  public function getVotesByLinkId($link_id) {
    $query = sprintf('SELECT votes FROM votes WHERE link_id = "%s"', $link_id);
    $result = $this->db->query($query);

    return unserialize($result->fetch_assoc()['votes']);
  }

  function getVotesByLinkIds($link_ids) {
    $query = sprintf('SELECT votes FROM votes WHERE link_id IN (%s)', $link_ids);
    $result = $this->db->query($query);

    while ($row = $result->fetch_assoc()) {
      $results[] = $row['votes'];
    }

    return $results;
  }

}
