$(document).ready(function() {

var urlGlobal = '';

chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT},
   function(tabs){
      getURL(tabs[0].url);
      urlGlobal = tabs[0].url;
   }
);

  function getURL(urlNews){
    $.ajax({
      type:'POST',
      url: 'http://hackathon.com.br/retrieve/news-status',
      data: {url: urlNews },
      crossDomain:true,
      success: function (data) {
        $('.message')
        .append(data);
/*
        var colorStatus = $('.message p span').first().text();
        var colorRGB;

        if(colorStatus == 'Confiavel') {
          chrome.browserAction.setBadgeBackgroundColor({color: [100,0,100,0]});
          console.log('1');
        } else if(colorStatus == 'Falso') {
          chrome.browserAction.setBadgeBackgroundColor({color: [244,67,54,0]});
          console.log('2');
        } else if(colorStatus == 'Sensacionalista') {
          chrome.browserAction.setBadgeBackgroundColor({color: [107,0,179,0]});
        } else if(colorStatus == 'Tendencioso'){
          chrome.browserAction.setBadgeBackgroundColor({color: [255,153,0,0]});
        } 

        chrome.browserAction.setBadgeText( { text: colorStatus } ); // Red, Green, Blue, Alpha 
      */

      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      },
    });
  }

  function sendStatus(urlNews, status, dataID){
    $.ajax({
      type:'POST',
      url: 'http://hackathon.com.br/update/vote',
      data: {url: urlNews, category: status },
      crossDomain:true,
      success: function (data) {
        disableBtn(dataID);
        $('.btn_selection')
        .append(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      },
    });
  }

  
  function disableBtn(dataID) {
    $(".btn-class").prop("disabled",true).addClass("disabled");
    $("#" + dataID).removeClass("disabled");
  }


  $(".btn-class").click(function() {
    var dataStatus = $(this).data("status");
    var dataID = $(this).attr("id");
    sendStatus(urlGlobal, dataStatus, dataID);
  });
 
});
