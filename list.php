<?php
function get_votes_by_link_ids($link_ids) {
    $squery = $db->prepare('SELECT votes FROM votes WHERE link_id IN (:link_ids)');
    $squery->bindParam(':link_ids', $link_ids, PDO::PARAM_STR, 255);
    $squery->execute();
    $result = $squery->fetchAll(PDO::FETCH_ASSOC);
}

function get_category_by_id($category_id) {
    $squery = $db->prepare('SELECT category FROM categories WHERE id = :id');
    $squery->bindParam(':id', $category_id);
    $squery->execute();
    
    $result = $squery->fetch(PDO::FETCH_ASSOC);
    
    return $result;
}

function get_infos_by_url($url) {
    // Carrega as informacoes da noticia.
    $link_id = get_link_id_by_url($url);
    $votes = get_votes_by_link_id($link_id);

    // Calcula as informacoes da noticia.
    $news_veracity = get_category_by_id(array_search(max($votes), $votes));
    
    
    // Carrega as informacoes do site.
    $url_parsed = parse_url($url);
    $domain = str_replace('www.', '', $url_parsed['host']);

    $squery = $db->prepare('SELECT id FROM links WHERE domain = :domain');
    $squery->bindParam(':domain', $domain, PDO::PARAM_STR, 255);
    $squery->execute();
    $result = $squery->fetchAll(PDO::FETCH_ASSOC);

    $link_ids = implode(',', $result);
    $votes = get_votes_by_link_ids($link_ids);

    $qty_votes = array();
    foreach ($votes as $vote) {
        $votes_per_link = unserialize($vote);
        foreach ($votes_per_link as $category_id => $qty) {
            $qty_votes[$category_id] = !empty($qty_votes[$category_id]) ? ($qty_votes[$category_id] + $qty) : $qty;
        }
    }

    // Calcula as informaçoes do site.
    $site_veracity = get_category_by_id(array_search(max($qty_votes), $qty_votes));

    
    $message = 'Este site foi avaliado como %s <br />';
    $message .= 'Este conteúdo foi avaliado como %s';

    return sprintf($message, $site_veracity, $news_veracity);
}